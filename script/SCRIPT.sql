Create table category(
	id INT NOT NULL AUTO_INCREMENT,
    parent INT,
	name VARCHAR(255) NOT NULL,    
	description VARCHAR(255),
	createDate timestamp,
	updateDate timestamp,
    PRIMARY KEY (id)
)

Create table page(
	id INT NOT NULL AUTO_INCREMENT,
    category INT,
	title VARCHAR(255) NOT NULL,    
	content BLOB,
	createDate timestamp,
	updateDate timestamp,
    PRIMARY KEY (id)
)
