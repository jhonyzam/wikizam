package br.com.wikizam.view;

import java.util.List;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import br.com.wikizam.category.CategoryNode;
import br.com.wikizam.page.Page;

public class WikiZamTreeModel2 implements TreeModel {

	private final CategoryNode root;

	public WikiZamTreeModel2(List<CategoryNode> roots) {
		root = new CategoryNode(null, "root", "dummy");
		root.setSubCategories(roots);
	}

	@Override
	public Object getRoot() {
		return root;
	}

	@Override
	public boolean isLeaf(Object node) {
		return node instanceof Page;
	}

	@Override
	public int getChildCount(Object parent) {
		CategoryNode node = (CategoryNode) parent;
		return node.getSubCategories().size() + node.getSubPages().size();
	}

	@Override
	public Object getChild(Object parent, int index) {
		CategoryNode node = (CategoryNode) parent;
		if (index < node.getSubCategories().size())
			return node.getSubCategories().get(index);
		return node.getSubPages().get(index - node.getSubCategories().size());
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		CategoryNode node = (CategoryNode) parent;
		if (node.getSubCategories().contains(child))
			return node.getSubCategories().indexOf(child);
		return node.getSubPages().indexOf(child) + node.getSubCategories().size();
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

}
