package br.com.wikizam.view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

import br.com.wikizam.exception.WikizamException;

public class AppWikiZam extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JTextPane panelCotent = new JTextPane();
	private final JLabel title = new JLabel("");

	public AppWikiZam() throws WikizamException {

		// Treview
		TreeWiki tree = new TreeWiki(this);

		// MENU
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(new MenuWiki(this, tree));

		// PANEL 1
		JPanel p1 = new JPanel();
		getContentPane().add(p1, BorderLayout.SOUTH);
		// PANEL 2
		JPanel p2 = new JPanel();
		getContentPane().add(p2, BorderLayout.NORTH);

		// Split
		JPanel panelRight = new JPanel();
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(tree), panelRight);
		splitPane.setResizeWeight(.1);

		panelCotent.setContentType("text/html");
		panelCotent.setEditable(false);

		panelRight.setLayout(new BoxLayout(panelRight, BoxLayout.Y_AXIS));
		panelRight.add(title);
		panelRight.add(panelCotent);

		this.getContentPane().add(splitPane);

		// APP
		this.setMinimumSize(new Dimension(800, 600));
		// this.setExtendedState(MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public JFrame getFrame() {
		return this;
	}

	public void setTitulo(String title) {
		this.title.setText(title);
	}

	public void setTextEditor(String content) {
		this.panelCotent.setText("<b>" + content + "</b>");
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					new AppWikiZam();
				} catch (WikizamException e) {
					e.printStackTrace();
				}
			}
		});
	}
}