package br.com.wikizam.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import br.com.wikizam.category.Category;
import br.com.wikizam.controller.CategoryController;
import br.com.wikizam.exception.WikizamException;

public class WindowCreateCategory extends JDialog {

	private static final long serialVersionUID = 1L;

	private JComboBox<Category> cbbParent = new JComboBox<>();
	private JTextField tfName = new JTextField();
	private JTextArea taDescription = new JTextArea(5, 1);
	private JTextField tfID = new JTextField();
	private JButton btnSave = new JButton("SALVAR");
	private JButton btnCancel = new JButton("CANCELAR");
	private TreeWiki tree;
	private JDialog myWindow;
	private boolean vSave = false;
	CategoryController catController = new CategoryController();

	public WindowCreateCategory(AppWikiZam app, TreeWiki tree, Integer id) {
		super(app, "CATEGORIA", true);		
		this.myWindow = this;
		this.tree = tree;
		this.windowPage();
		this.vSave = false;

		if (id > 0)
			loadCategory(id);

		this.setResizable(false);
		this.setSize(450, 400);
		this.setLocationRelativeTo(null);
	}

	public void windowPage() {
		JPanel p1 = new JPanel();

		p1.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTHWEST;

		p1.add(getLabel(new JLabel("Parent:"), c, 0), c);
		p1.add(getCombobox(cbbParent, c, 1), c);

		p1.add(getLabel(new JLabel("Nome:"), c, 2), c);
		p1.add(getTextField(tfName, c, 3), c);

		p1.add(getLabel(new JLabel("Descri��o:"), c, 4), c);
		p1.add(getTextArea(taDescription, c, 5), c);

		JPanel p2 = getPanelID(c);
		c.gridx = 0;
		c.gridy = 6;
		c.insets = new Insets(10, 5, 0, 300);
		p1.add(p2, c);

		// BUTTON
		c.insets = new Insets(10, 5, 0, 105);
		p1.add(getButton(btnSave, c, 6), c);

		c.insets = new Insets(10, 5, 0, 5);
		p1.add(getButton(btnCancel, c, 6), c);

		// SALVAR
		btnSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int id = 0;
				if (!tfID.getText().isEmpty())
					id = Integer.parseInt(tfID.getText());

				int parent = ((Category) cbbParent.getSelectedItem()).getId();
				String name = tfName.getText();
				String description = taDescription.getText();

				Category newCategory = new Category(parent, name, description, id);
				vSave = true;
				try {
					catController.Save(newCategory);
				} catch (WikizamException e1) {
					throw new RuntimeException("Erro ao tentar salvar categoria" + e);
				}
			}

		});

		// CANCELAR
		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				tfName.setText("");
				taDescription.setText("");
				myWindow.setVisible(false);
			}

		});

		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentHidden(ComponentEvent e) {
				try {
					if (vSave) {
						tree.reload();						
					}
				} catch (WikizamException e1) {
					throw new RuntimeException("Erro ao atualizar treeview: " + e1.getMessage());
				}
			}
		});

		this.add(p1, BorderLayout.NORTH);
	}

	public JTextField getTextField(JTextField tf, GridBagConstraints c, int grdy) {
		tf.setFont(tf.getFont().deriveFont(14f));
		c.ipady = 8;
		c.ipadx = 8;
		c.insets = new Insets(0, 5, 0, 5);
		c.weightx = 0.5;
		c.gridy = grdy;
		tf.setBorder(BorderFactory.createLineBorder(Color.GRAY));

		return tf;
	}

	public JScrollPane getTextArea(JTextArea ta, GridBagConstraints c, int grdy) {
		ta.setFont(ta.getFont().deriveFont(14f));
		c.ipady = 8;
		c.ipadx = 8;
		c.insets = new Insets(0, 5, 0, 5);
		c.weightx = 0.5;
		c.gridy = grdy;

		JScrollPane scrollpane = new JScrollPane(ta);

		return scrollpane;
	}

	// FIXO
	public JPanel getPanelID(GridBagConstraints c) {
		tfID.setFont(tfID.getFont().deriveFont(14f));
		c.ipady = 8;
		c.ipadx = 8;
		c.weightx = 0;
		c.gridy = 0;

		JPanel p2 = new JPanel();
		p2.setLayout(new GridBagLayout());
		p2.add(getLabel(new JLabel("ID:"), c, 0));
		p2.add(tfID, c);

		tfID.setEditable(false);

		return p2;
	}

	@SuppressWarnings("unchecked")
	public JComboBox<Category> getCombobox(JComboBox<Category> cbb, GridBagConstraints c, int grdy) {
		cbb = new CategoryController().readAllCategories(cbb);
		cbb.setFont(cbb.getFont().deriveFont(14f));
		cbb.setRenderer(new BasicComboBoxRenderer() {
			private static final long serialVersionUID = 1L;

			@SuppressWarnings("rawtypes")
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
					boolean cellHasFocus) {

				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

				if (value instanceof Category) {
					setText(((Category) value).getName());
				}

				return this;
			}

		});

		c.ipady = 8;
		c.insets = new Insets(0, 5, 0, 5);
		c.weightx = 0.5;
		c.gridy = grdy;

		return cbb;
	}

	public JLabel getLabel(JLabel lbl, GridBagConstraints c, int grdy) {
		c.insets = new Insets(0, 5, 0, 5);
		c.weightx = 0.5;
		c.gridy = grdy;

		return lbl;
	}

	public JButton getButton(JButton btn, GridBagConstraints c, int grdy) {
		c.gridx = 0;
		c.gridy = 6;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.NORTHEAST;

		return btn;
	}

	public void loadCategory(Integer id) {
		Category cat = catController.readCategory(id);

		if (cat.getParent() > 0) {
			Category parent = catController.readCategory(cat.getParent());
			cbbParent.setSelectedItem(parent);
		} else
			cbbParent.setSelectedIndex(0);

		tfName.setText(cat.getName());
		taDescription.setText(cat.getDescription());
		tfID.setText(id.toString());
	}

}
