package br.com.wikizam.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MenuWiki extends JMenu {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MenuWiki(AppWikiZam app, TreeWiki tree) {
		super("Cadastros");
		createMenuCategory(app, tree);
		createMenuPage(app, tree);
	}

	public void createMenuCategory(AppWikiZam app, TreeWiki tree) {
		// Itens
		JMenuItem menuItem = new JMenuItem("ADD Categoria", KeyEvent.VK_T);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));

		menuItem.addActionListener(new ActionListener() {

			private WindowCreateCategory dialog;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (dialog == null) // primeira vez
					dialog = new WindowCreateCategory(app, tree, 0);

				dialog.setVisible(true);
				dialog = null;
			}

		});

		this.add(menuItem);
	}

	public void createMenuPage(AppWikiZam app, TreeWiki tree) {
		// Itens
		JMenuItem menuItem = new JMenuItem("ADD Pagina", KeyEvent.VK_T);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));

		menuItem.addActionListener(new ActionListener() {

			private WindowCreatePage dialog;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (dialog == null)
					dialog = new WindowCreatePage(app, tree, 0);

				dialog.setVisible(true);
				dialog = null;
			}
		});

		this.add(menuItem);
	}

}
