package br.com.wikizam.view;

import java.util.List;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import br.com.wikizam.category.Category;
import br.com.wikizam.page.Page;

public class WikiZamTreeModel implements TreeModel {

	private static final String ROOT = "ROOTZAM";
	private final List<Category> categories;
	private final List<Page> pages;

	public WikiZamTreeModel(List<Category> categories, List<Page> pages) {
		this.categories = categories;
		this.pages = pages;
	}

	@Override
	public Object getRoot() {
		return ROOT;
	}

	@Override
	public boolean isLeaf(Object node) {
		return node instanceof Page;
	}

	@Override
	public int getChildCount(Object parent) {
		// ROOT
		if (parent == ROOT) {
			int soma = 0;
			for (Category cat : categories) {
				if (cat.getParent() == 0)
					soma++;
			}
			return soma;
		}

		// Categoria
		if (parent instanceof Category) {
			Category pai = (Category) parent;
			int soma = 0;
			for (Category filho : categories) {
				if (filho.getParent() == pai.getId())
					soma++;
			}
			for (Page filho : pages) {
				if (filho.getCategory() == pai.getId())
					soma++;
			}
			return soma;
		}

		// Page
		if (parent instanceof Page) {
			return 0;
		}

		throw new IllegalArgumentException("Tipo n�o reconhecido:  " + parent);
	}

	@Override
	public Object getChild(Object parent, int index) {

		if (parent == ROOT) {
			for (Category cat : categories) {
				if (cat.getParent() == 0) {
					if (index-- == 0)
						return cat;
				}
			}
		}

		// Categoria
		if (parent instanceof Category) {
			Category pai = (Category) parent;

			for (Category filho : categories) {
				if (filho.getParent() == pai.getId()) {
					if (index == 0)
						return filho;
					index--;
				}
			}

			for (Page filho : pages) {
				if (filho.getCategory() == pai.getId()) {
					if (index == 0)
						return filho;
					index--;
				}
			}
		}

		throw new IllegalArgumentException("Tipo n�o reconhecido:  " + parent);
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		int index = 0;

		if (parent == ROOT) {
			for (Category cat : categories) {
				if (cat.getParent() == 0) {
					if (cat == child)
						return index;
					index++;
				}
			}
		}

		// Categoria
		if (parent instanceof Category) {
			Category pai = (Category) parent;

			for (Category filho : categories) {
				if (filho.getParent() == pai.getId()) {
					if (filho == child)
						return index;
					index++;
				}
			}

			for (Page filho : pages) {
				if (filho.getCategory() == pai.getId()) {
					if (filho == child)
						return index;
					index++;
				}
			}
		}

		throw new IllegalArgumentException("Tipo n�o reconhecido:  " + parent);
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

}
