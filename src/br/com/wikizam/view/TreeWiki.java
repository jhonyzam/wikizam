package br.com.wikizam.view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeCellRenderer;

import br.com.wikizam.category.Category;
import br.com.wikizam.category.CategoryDAO;
import br.com.wikizam.controller.CategoryController;
import br.com.wikizam.controller.PageController;
import br.com.wikizam.exception.WikizamException;
import br.com.wikizam.page.Page;
import br.com.wikizam.page.PageDAO;

public class TreeWiki extends JTree {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WikiZamTreeModel model;
	private CategoryController catController = new CategoryController();
	private PageController pagController = new PageController();

	CategoryDAO daoC = new CategoryDAO();
	PageDAO daoP = new PageDAO();

	public TreeWiki(AppWikiZam app) throws WikizamException {
		List<Category> categories = daoC.readAll();
		List<Page> pages = daoP.readAll();

		model = new WikiZamTreeModel(categories, pages);

		this.setModel(model);

		this.setCellRenderer(new DefaultTreeCellRenderer() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
					boolean leaf, int row, boolean hasFocus) {

				super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

				if (value instanceof Category) {
					setText(((Category) value).getName());
				}

				if (value instanceof Page) {
					setText(((Page) value).getTitle());
				}

				return this;
			}
		});

		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Object node = ((JTree) e.getComponent()).getLastSelectedPathComponent();
				TreeWiki tree = ((TreeWiki) e.getComponent());

				if (SwingUtilities.isRightMouseButton(e)) {
					if (node instanceof Page) {
						JPopupMenu popup = new JPopupMenu();
						// MENU 1
						JMenuItem menuitem1 = new JMenuItem("Editar Pagina");
						popup.add(menuitem1);

						menuitem1.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								WindowCreatePage dialog = null;
								if (dialog == null)
									dialog = new WindowCreatePage(app, tree, ((Page) node).getId());

								dialog.setVisible(true);
							}
						});

						// MENU 2
						JMenuItem menuitem2 = new JMenuItem("Excluir Pagina");
						popup.add(menuitem2);

						menuitem2.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								String message = "Deseja realmente excluir a pagina?";
								String title = "Confirmação";
								int reply = JOptionPane.showConfirmDialog(null, message, title,
										JOptionPane.YES_NO_OPTION);
								if (reply == JOptionPane.YES_OPTION) {
									try {
										pagController.Delete((Page) node);
										reload();
									} catch (WikizamException e1) {
										throw new RuntimeException("Erro ao tentar excluir a pagina: " + e1);
									}
								}
							}
						});

						popup.show(e.getComponent(), e.getX(), e.getY());

					} else if (node instanceof Category) {
						JPopupMenu popup = new JPopupMenu();
						// MENU 1
						JMenuItem menuitem1 = new JMenuItem("Editar Categoria");
						popup.add(menuitem1);

						menuitem1.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								WindowCreateCategory dialog = null;
								if (dialog == null)
									dialog = new WindowCreateCategory(app, tree, ((Category) node).getId());

								dialog.setVisible(true);
							}
						});

						// MENU 2
						JMenuItem menuitem2 = new JMenuItem("Excluir Categoria");
						popup.add(menuitem2);

						menuitem2.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								String message = "Deseja realmente excluir a categoria?";
								String title = "Confirmação";
								int reply = JOptionPane.showConfirmDialog(null, message, title,
										JOptionPane.YES_NO_OPTION);
								if (reply == JOptionPane.YES_OPTION) {
									try {
										catController.Delete((Category) node);
										reload();
									} catch (WikizamException e1) {
										throw new RuntimeException("Erro ao tentar excluir a categoria: " + e1);
									}
								}
							}
						});

						popup.show(e.getComponent(), e.getX(), e.getY());

					}

				} else {
					if (e.getClickCount() == 2) {
						if (node instanceof Page) {
							Page page = (Page) node;
							app.setTitulo(page.getTitle());
							app.setTextEditor(page.getContent());
						}
					}
				}
			}
		});

		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.setRootVisible(false);
	}

	public void reload() throws WikizamException {
		List<Category> categories = daoC.readAll();
		List<Page> pages = daoP.readAll();

		model = new WikiZamTreeModel(categories, pages);

		this.setModel(model);
	}
}
