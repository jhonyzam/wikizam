package br.com.wikizam.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.wikizam.exception.WikizamException;

public class ConnectionManager {

	private static final String DATABASE = "wikizam";
	private static final String IP = "localhost";	
	private static final String STR_CON = "jdbc:mysql://" + IP + ":3306/" + DATABASE
			+ "?verifyServerCertificate=true&useSSL=true&requireSSL=false&zeroDateTimeBehavior=convertToNull";
	private static final String USER = "root";
	private static final String PASSWORD = "admin";

	public static Connection getConnection() throws WikizamException {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(STR_CON, USER, PASSWORD);
//			System.out.println("[ConnectionManager]: Obtendo conexao");
		} catch (SQLException e) {
			throw new WikizamException("Erro ao obter a conexao", e);
		}
		return conn;
	}

	public static void closeAll(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.out.println("---> Nao foi possivel fechar a conexao com o banco");
			System.out.println("---> Detalhamento do erro: ");
			e.printStackTrace();
		}
	}

	public static void closeAll(Connection conn, Statement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
				closeAll(conn);
			}
		} catch (Exception e) {
			System.out.println("---> Nao foi possivel fechar a conexao com o banco");
			System.out.println("---> Detalhamento do erro: ");
			e.printStackTrace();
		}
	}

	public static void closeAll(Connection conn, Statement stmt, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
				closeAll(conn, stmt);
			}
		} catch (Exception e) {
			System.out.println("---> Nao foi possivel fechar a conexao com o banco");
			System.out.println("---> Detalhamento do erro: ");
			e.printStackTrace();
		}
	}
}
