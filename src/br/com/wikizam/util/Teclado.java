package br.com.wikizam.util;

import java.util.Scanner;

public class Teclado {

	private static Scanner scanner;

	public static String le() {
		scanner = new Scanner(System.in);
		return scanner.nextLine();
	}
}
