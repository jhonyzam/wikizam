package br.com.wikizam.page;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.com.wikizam.dao.DAO;
import br.com.wikizam.exception.WikizamException;
import br.com.wikizam.util.ConnectionManager;

public class PageDAO implements DAO<Page> {
	private final static String SALVAR = "INSERT INTO page (category, title, content) VALUES (?,?,?)";
	private final static String DELETE = "DELETE FROM page WHERE id = ?";
	private final static String UPDATE = "UPDATE page SET category = ?, title = ?, content = ? WHERE id = ?";
	private final static String GET_ALL = "SELECT * FROM page";
	private final static String GET_BY_ID = "SELECT * FROM page WHERE id = ?";

	public Page create(Page page) throws WikizamException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(SALVAR, Statement.RETURN_GENERATED_KEYS)) {

			stmt.setInt(1, page.getCategory());
			stmt.setString(2, page.getTitle());
			stmt.setString(3, page.getContent());
			int row = stmt.executeUpdate();

			if (row > 0) {
				final ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					page.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new WikizamException("Erro ao inserir pagina: (" + e.getMessage() + ")", e);
		}

		return page;
	}

	// ALTERAR REGISTRO
	public Page update(Page page) throws WikizamException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(UPDATE)) {

			stmt.setInt(1, page.getCategory());
			stmt.setString(2, page.getTitle());
			stmt.setString(3, page.getContent());
			stmt.setInt(4, page.getId());

			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new WikizamException("Erro ao alterar pagina: (" + e.getMessage() + ")", e);
		}

		return page;
	}

	// LER TODOS OS REGISTROS
	public List<Page> readAll() throws WikizamException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(GET_ALL);
				ResultSet rs = stmt.executeQuery()) {

			//
			List<Page> pages = new LinkedList<>();
			Page page = null;

			while (rs.next()) {
				page = new Page(rs.getInt("category"), rs.getString("title"), rs.getString("content"),
						rs.getTimestamp("createDate"), rs.getTimestamp("updateDate"), rs.getInt("id"));

				pages.add(page);
			}

			return pages;

		} catch (SQLException e) {
			throw new WikizamException("Erro ao listar todas paginas: (" + e.getMessage() + ")", e);
		}
	}

	// LER 1 REGISTRO
	public Page read(int id) throws WikizamException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(GET_BY_ID)) {

			Page page = null;
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				page = new Page(rs.getInt("category"), rs.getString("title"), rs.getString("content"),
						rs.getTimestamp("createDate"), rs.getTimestamp("updateDate"), rs.getInt("id"));
			}

			return page;

		} catch (SQLException e) {
			throw new WikizamException("Erro ao listar 1 pagina: (" + e.getMessage() + ")", e);
		}
	}

	public boolean delete(int id) throws WikizamException {
		int row = 0;
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(DELETE)) {
			stmt.setInt(1, id);
			row = stmt.executeUpdate();
		} catch (SQLException e) {
			throw new WikizamException("Erro ao remover pagina: (" + e.getMessage() + ")", e);
		}

		return (row > 0);
	}

}