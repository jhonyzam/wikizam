package br.com.wikizam.page;

import java.io.IOException;
import java.util.List;

import br.com.wikizam.exception.WikizamException;
import br.com.wikizam.util.Teclado;

public class TestePageDAO {

	public static void montaMenu() {
		System.out.println("----------- Menu para inserir Paginas --------------");
		System.out.println("Inserir pagina  : 1");
		System.out.println("Remover pagina  : 2");
		System.out.println("Listar pagina   : 3");
		System.out.println("Buscar pagina   : 4");
		System.out.println("Alterar pagina  : 5");
		System.out.println("FINALIZAR       : 0");
		System.out.println("-------------------------");
		System.out.print("ESCOLHA A OPCAO: ");
	}

	public static int leOperacao() throws IOException, NumberFormatException {
		String op = Teclado.le();
		int operacao = Integer.parseInt(op);
		return operacao;
	}

	public static Page lePage() {
		Page page = null;
		try {
			System.out.print("ID da categoria: ");
			int category = Integer.valueOf(Teclado.le());

			System.out.print("Titulo da pagina: ");
			String title = Teclado.le();

			System.out.print("Conteudo da pagina: ");
			String content = Teclado.le();

			page = new Page(category, title, content);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return page;
	}

	// Sobrecarga de lePage
	public static Page lePage(Page page) {
		try {
			System.out.print("ID da categoria: ");
			page.setCategory(Integer.valueOf(Teclado.le()));

			System.out.print("Titulo da pagina: ");
			page.setTitle(Teclado.le());

			System.out.print("Conteudo da pagina: ");
			page.setContent(Teclado.le());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return page;
	}

	public static String leIDPage() {
		String id = "";
		try {
			while (id.equals("")) {
				System.out.print("ID da pagina : ");
				String strId = Teclado.le();
				if (strId != null && strId.length() > 0) {
					id = strId;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}

	// Vai mostrar as Paginas no console
	public static void listarPages(List<Page> pages) {
		if (pages != null) {
			for (int i = 0; i < pages.size(); i++) {
				System.out.println(((Page) pages.get(i)));
			}
		}
	}

	public static void executarTarefa(int operacao) throws WikizamException {
		String pageID = "";
		Page page = null;
		PageDAO pageDAO = new PageDAO();

		switch (operacao) {
		case 1:
			System.out.println("Inserindo pagina");
			page = lePage();
			pageDAO.create(page);
			break;
		case 2:
			System.out.println("Removendo pagina");
			pageID = leIDPage();
			pageDAO.delete(Integer.parseInt(pageID));
			break;
		case 3:
			System.out.println("Listando paginas");
			List<Page> pages = pageDAO.readAll();
			listarPages(pages);
			break;
		case 4:
			System.out.println("Listando paginas por id");
			pageID = leIDPage();
			page = pageDAO.read(Integer.parseInt(pageID));
			if (page != null)
				System.out.println(page);
			else
				System.out.println("ID nao encontrado!");
			break;
		case 5:
			pageID = leIDPage();
			page = pageDAO.read(Integer.parseInt(pageID));
			System.out.println("Alterando a pagina");
			page = lePage(page);
			pageDAO.update(page);
			break;
		}
	}

	public static void main(String[] args) throws WikizamException {
		int TERMINAR = 0;
		int operacao = 1;
		do {
			try {
				montaMenu();
				operacao = leOperacao();
				if (operacao == TERMINAR) {
					break;
				} else {
					executarTarefa(operacao);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				System.out.println("Operacao invalida");
			}
		} while (operacao != TERMINAR);
	}
}
