package br.com.wikizam.page;

import java.sql.Timestamp;
import java.util.Date;

import br.com.wikizam.category.CategoryNode;

/**
 * @author Jhonatan Reis
 *
 */
public class PageNode {
	private Integer id;
	private CategoryNode category;
	private String title;
	private String content;
	private Date createDate;
	private Date updateDate;

	public PageNode(CategoryNode category, String tile, String content) {
		setCategory(category);
		setTitle(tile);
		setContent(content);
	}

	public PageNode(CategoryNode category, String tile, String content, int id) {
		this(category, tile, content);
		setId(id);
	}

	public PageNode(CategoryNode category, String tile, String content, Timestamp createDate, Timestamp updateDate, int id) {
		this(category, tile, content);
		setCreateDate(createDate);
		setUpdateDate(updateDate);
		setId(id);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CategoryNode getCategory() {
		return this.category;
	}

	public void setCategory(CategoryNode category) {
		this.category = category;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Page [id=" + id + ", category=" + category + ", title=" + title + ", content=" + content + "]";
	}

}