package br.com.wikizam.page;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.wikizam.dao.DAO;

public class PageMemory implements DAO<Page> {
	private static int seq;
	private static Map<Integer, Page> dadosMap = new HashMap<>();

	public Page create(Page page) {
		page.setId(++seq);
		page.setCreateDate(new Date());

		dadosMap.put(page.getId(), page);

		return page;
	}

	public Page read(int id) {
		return dadosMap.get(id);
	}

	public List<Page> readAll() {
		return new ArrayList<>(dadosMap.values());
	}

	public Page update(Page page) {
		dadosMap.put(page.getId(), page);
		
		return page;
	}

	public boolean delete(int id) {
		return dadosMap.remove(id) != null;
	}

}