package br.com.wikizam.page;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Jhonatan Reis
 *
 */
public class Page {
	private Integer id;
	private Integer category;
	private String title;
	private String content;
	private Date createDate;
	private Date updateDate;

	public Page(int category, String tile, String content) {
		setCategory(category);
		setTitle(tile);
		setContent(content);
	}

	public Page(int category, String tile, String content, int id) {
		this(category, tile, content);
		setId(id);
	}

	public Page(int category, String tile, String content, Timestamp createDate, Timestamp updateDate, int id) {
		this(category, tile, content);
		setCreateDate(createDate);
		setUpdateDate(updateDate);
		setId(id);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategory() {
		return this.category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Page [id=" + id + ", category=" + category + ", title=" + title + ", content=" + content + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Page other = (Page) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}