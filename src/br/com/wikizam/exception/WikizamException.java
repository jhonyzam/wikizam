package br.com.wikizam.exception;

public class WikizamException extends Exception {

	private static final long serialVersionUID = 1L;

	public WikizamException(String mensagem, Exception e) {
		super(mensagem, e);
	}

	public WikizamException(String mensagem) {
		super(mensagem);
	}
}
