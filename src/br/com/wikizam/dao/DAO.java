package br.com.wikizam.dao;

import java.util.List;

import br.com.wikizam.exception.WikizamException;

public interface DAO<T> {
	T create(T obj) throws WikizamException;

	T read(int id) throws WikizamException;

	List<T> readAll() throws WikizamException;

	T update(T obj) throws WikizamException;

	boolean delete(int id) throws WikizamException;
}