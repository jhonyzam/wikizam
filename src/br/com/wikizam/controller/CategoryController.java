package br.com.wikizam.controller;

import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import br.com.wikizam.category.Category;
import br.com.wikizam.category.CategoryDAO;
import br.com.wikizam.exception.WikizamException;

public class CategoryController {

	private CategoryDAO categoryDAO = new CategoryDAO();

	// ReadAll
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JComboBox<Category> readAllCategories(JComboBox<Category> cbb) {
		List<Category> categories;

		try {
			categories = categoryDAO.readAll();
		} catch (WikizamException e) {
			throw new RuntimeException("Erro ao popular combobox: " + e);
		}

		cbb.removeAllItems();

		Category catNull = new Category(0, "", "", 0);
		categories.add(0, catNull);
		cbb.setModel(new DefaultComboBoxModel(categories.toArray()));

		return cbb;
	}

	public Category readCategory(Integer id) {
		Category category;

		try {
			category = categoryDAO.read(id);
		} catch (WikizamException e) {
			throw new RuntimeException("Erro ao buscar categoria " + id + ": " + e);
		}

		return category;
	}

	// Save
	public boolean Save(Category cat) throws WikizamException {
		if (cat.getId() > 0) {
			categoryDAO.update(cat);
		} else {
			categoryDAO.create(cat);
		}

		return true;
	}

	// Delete
	public boolean Delete(Category cat) throws WikizamException {
		categoryDAO.delete(cat.getId());

		return true;
	}
}
