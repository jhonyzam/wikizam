package br.com.wikizam.controller;

import br.com.wikizam.exception.WikizamException;
import br.com.wikizam.page.Page;
import br.com.wikizam.page.PageDAO;

public class PageController {

	private PageDAO pageDAO = new PageDAO();

	public Page readPage(Integer id) {
		Page page;

		try {
			page = pageDAO.read(id);
		} catch (WikizamException e) {
			throw new RuntimeException("Erro ao buscar pagina " + id + ": " + e);
		}

		return page;
	}

	// Save
	public boolean Save(Page pag) throws WikizamException {
		if (pag.getId() > 0) {
			pageDAO.update(pag);
		} else {
			pageDAO.create(pag);
		}

		return true;
	}

	// Delete
	public boolean Delete(Page pag) throws WikizamException {
		pageDAO.delete(pag.getId());

		return true;
	}
}
