package br.com.wikizam.category;

import java.io.IOException;
import java.util.List;

import br.com.wikizam.exception.WikizamException;
import br.com.wikizam.util.Teclado;

public class TesteCategoryDAO {

	public static void montaMenu() {
		System.out.println("----------- Menu para inserir Categorias --------------");
		System.out.println("Inserir categoria  : 1");
		System.out.println("Remover categoria  : 2");
		System.out.println("Listar categoria   : 3");
		System.out.println("Buscar categoria   : 4");
		System.out.println("Alterar categoria  : 5");
		System.out.println("FINALIZAR          : 0");
		System.out.println("-------------------------");
		System.out.print("ESCOLHA A OPCAO: ");
	}

	public static int leOperacao() throws IOException, NumberFormatException {
		String op = Teclado.le();
		int operacao = Integer.parseInt(op);
		return operacao;
	}

	public static Category leCategory() {
		Category cat = null;
		try {
			System.out.print("ID da categoria pai: ");
			int parent = Integer.valueOf(Teclado.le());

			System.out.print("Nome do categoria: ");
			String name = Teclado.le();

			System.out.print("Descricao da categoria: ");
			String description = Teclado.le();

			cat = new Category(parent, name, description);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cat;
	}

	// Sobrecarga de leCategory
	public static Category leCategory(Category cat) {
		try {
			System.out.print("ID da categoria pai: ");
			cat.setParent(Integer.valueOf(Teclado.le()));

			System.out.print("Nome da categoria: ");
			cat.setName(Teclado.le());

			System.out.print("Descricao da categoria: ");
			cat.setDescription(Teclado.le());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return cat;
	}

	public static String leIDCategory() {
		String id = "";
		try {
			while (id.equals("")) {
				System.out.print("ID da categoria : ");
				String strId = Teclado.le();
				if (strId != null && strId.length() > 0) {
					id = strId;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}

	// Vai mostrar as Categorias no console
	public static void listarCategorias(List<Category> categorias) {
		if (categorias != null) {
			for (int i = 0; i < categorias.size(); i++) {
				System.out.println(((Category) categorias.get(i)));
			}
		}
	}

	public static void executarTarefa(int operacao) throws WikizamException {
		String catID = "";
		Category cat = null;
		CategoryDAO categoryDAO = new CategoryDAO();

		switch (operacao) {
		case 1:
			System.out.println("Inserindo categoria");
			cat = leCategory();
			categoryDAO.create(cat);
			break;
		case 2:
			System.out.println("Removendo categoria");
			catID = leIDCategory();
			categoryDAO.delete(Integer.parseInt(catID));
			break;
		case 3:
			System.out.println("Listando categorias");
			List<Category> categories = categoryDAO.readAll();
			listarCategorias(categories);
			break;
		case 4:
			System.out.println("Listando categoria por id");
			catID = leIDCategory();
			cat = categoryDAO.read(Integer.parseInt(catID));
			if (cat != null)
				System.out.println(cat);
			else
				System.out.println("ID nao encontrado!");

			break;
		case 5:
			catID = leIDCategory();
			cat = categoryDAO.read(Integer.parseInt(catID));
			System.out.println("Alterando a categoria");
			cat = leCategory(cat);
			categoryDAO.update(cat);
			break;
		}
	}

	public static void main(String[] args) throws WikizamException {
		int TERMINAR = 0;
		int operacao = 1;
		do {
			try {
				montaMenu();
				operacao = leOperacao();
				if (operacao == TERMINAR) {
					break;
				} else {
					executarTarefa(operacao);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				System.out.println("Operacao invalida");
			}
		} while (operacao != TERMINAR);
	}
}
