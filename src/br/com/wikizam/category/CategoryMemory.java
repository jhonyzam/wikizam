package br.com.wikizam.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.wikizam.dao.DAO;

public class CategoryMemory implements DAO <Category> {	
	private static int seq;
	private static Map<Integer, Category> dadosMap = new HashMap<>();
	
	public Category create(Category category){						
		category.setId(++seq);		
//		category.setCreateDate(new Date());		
		dadosMap.put(category.getId(), category);
		
		return category;		
	}
	
	public Category read(int id){
		return dadosMap.get(id);
		
	}
	
	public List<Category> readAll(){		
		return new ArrayList<>(dadosMap.values());
	}
	
	public Category update(Category category){
		dadosMap.put(category.getId(), category);
		
		return category;
	}
	
	public boolean delete(int id){
		return dadosMap.remove(id) != null;
	}
	
}