package br.com.wikizam.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.com.wikizam.dao.DAO;
import br.com.wikizam.exception.WikizamException;
import br.com.wikizam.util.ConnectionManager;

public class CategoryDAO implements DAO<Category> {		
	private final static String SALVAR = "INSERT INTO category (parent, name, description) VALUES (?,?,?)";
	private final static String DELETE = "DELETE FROM category WHERE id = ?";
	private final static String UPDATE = "UPDATE category SET parent = ?, name = ?, description = ? WHERE id = ?";
	private final static String GET_ALL = "SELECT * FROM category";
	private final static String GET_BY_ID = "SELECT * FROM category WHERE id = ?";

	// CRIAR REGISTROS
	public Category create(Category category) throws WikizamException {

		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(SALVAR, Statement.RETURN_GENERATED_KEYS)) {

			stmt.setInt(1, category.getParent());
			stmt.setString(2, category.getName());
			stmt.setString(3, category.getDescription());
			int row = stmt.executeUpdate();

			if (row > 0) {
				final ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					category.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new WikizamException("Erro ao inserir categoria: (" + e.getMessage() + ")", e);
		}

		return category;
	}

	// ALTERAR REGISTRO
	public Category update(Category category) throws WikizamException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(UPDATE)) {

			stmt.setInt(1, category.getParent());
			stmt.setString(2, category.getName());
			stmt.setString(3, category.getDescription());
			stmt.setInt(4, category.getId());

			stmt.executeUpdate();			
		} catch (SQLException e) {
			throw new WikizamException("Erro ao alterar categoria: (" + e.getMessage() + ")", e);
		}

		return category;
	}

	// LER TODOS OS REGISTROS
	public List<Category> readAll() throws WikizamException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(GET_ALL);
				ResultSet rs = stmt.executeQuery()) {

			//
			List<Category> categories = new LinkedList<>();
			Category cat = null;

			while (rs.next()) {
				cat = new Category(rs.getInt("parent"), rs.getString("name"), rs.getString("description"),
						rs.getTimestamp("createDate"), rs.getTimestamp("updateDate"), rs.getInt("id"));

				categories.add(cat);
			}

			return categories;

		} catch (SQLException e) {
			throw new WikizamException("Erro ao listar todas categorias: (" + e.getMessage() + ")", e);
		}
	}

	// LER 1 REGISTRO
	public Category read(int id) throws WikizamException {
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(GET_BY_ID)) {

			Category cat = null;
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				cat = new Category(rs.getInt("parent"), rs.getString("name"), rs.getString("description"),
						rs.getTimestamp("createDate"), rs.getTimestamp("updateDate"), rs.getInt("id"));
			}

			return cat;

		} catch (SQLException e) {
			throw new WikizamException("Erro ao listar 1 categoria: (" + e.getMessage() + ")", e);
		}
	}

	// DELETAR
	public boolean delete(int id) throws WikizamException {
		int row = 0;
		try (Connection conn = ConnectionManager.getConnection();
				PreparedStatement stmt = conn.prepareStatement(DELETE)) {
			stmt.setInt(1, id);
			row = stmt.executeUpdate();
		} catch (SQLException e) {
			throw new WikizamException("Erro ao remover categoria: (" + e.getMessage() + ")", e);
		}

		return (row > 0);
	}
}