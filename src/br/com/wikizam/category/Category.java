package br.com.wikizam.category;

import java.sql.Timestamp;

public class Category {
	private Integer id;
	private Integer parent;
	private String name;
	private String description;
	private Timestamp createDate;
	private Timestamp updateDate;

	public Category(int parent, String name, String description) {
		setParent(parent);
		setName(name);
		setDescription(description);
	}

	public Category(int parent, String name, String description, int id) {
		this(parent, name, description);
		setId(id);
	}

	public Category(int parent, String name, String description, Timestamp createDate, Timestamp updateDate, int id) {
		this(parent, name, description);
		setCreateDate(createDate);
		setUpdateDate(updateDate);
		setId(id);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getParent() {
		return this.parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", parent=" + parent + ", name=" + name + ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}