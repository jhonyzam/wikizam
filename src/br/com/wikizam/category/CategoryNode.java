package br.com.wikizam.category;

import java.sql.Timestamp;
import java.util.List;

import br.com.wikizam.page.PageNode;

public class CategoryNode {
	private Integer id;
	private CategoryNode parent;
	private String name;
	private String description;
	private Timestamp createDate;
	private Timestamp updateDate;
	private List<CategoryNode> subCategories;
	private List<PageNode> subPages;

	public CategoryNode(CategoryNode parent, String name, String description) {
		setParent(parent);
		setName(name);
		setDescription(description);
	}

	public CategoryNode(CategoryNode parent, String name, String description, int id) {
		this(parent, name, description);
		setId(id);
	}

	public CategoryNode(CategoryNode parent, String name, String description, Timestamp createDate, Timestamp updateDate, int id) {
		this(parent, name, description);
		setCreateDate(createDate);
		setUpdateDate(updateDate);
		setId(id);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CategoryNode getParent() {
		return this.parent;
	}

	public void setParent(CategoryNode parent) {
		this.parent = parent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	
	public List<CategoryNode> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(List<CategoryNode> subCategories) {
		this.subCategories = subCategories;
	}

	public List<PageNode> getSubPages() {
		return subPages;
	}

	public void setSubPages(List<PageNode> subPages) {
		this.subPages = subPages;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", parent=" + parent + ", name=" + name + ", description=" + description + "]";
	}

}