package br.com.wikizam;

import java.sql.Connection;

import br.com.wikizam.exception.WikizamException;
import br.com.wikizam.util.ConnectionManager;

public class WikizamApp {

	public static void main(String[] args) throws WikizamException {
		
		Connection conn = null;
		try {
			conn = ConnectionManager.getConnection();
		} finally {
			ConnectionManager.closeAll(conn);
		}

		// *************CATEGORY **************
		// System.out.println("CATEGORY\n\n");
		// Category cat1 = new Category();
		// // cat1.setParent(0);
		// cat1.setName("Categoria 1");
		// cat1.setDescription("Descricao da Categoria 1");
		//
		// Category cat2 = new Category();
		// // cat2.setParent(0);
		// cat2.setName("Categoria 1");
		// cat2.setDescription("Descricao da Categoria 1");
		//
		// CategoryDAO categoryDAO = new CategoryDAO();
		//
		// // System.out.println("CREATE");
		// categoryDAO.create(cat1);
		// categoryDAO.create(cat2);
		//
		// System.out.println("READ ALL");
		// List<Category> CatReadAll = categoryDAO.readAll();
		// for (Category cat : CatReadAll) {
		// System.out.println(cat);
		// }
		//
		// System.out.println("READ");
		// System.out.println(categoryDAO.read(2));
		//
		// System.out.println("UPDATE");
		// cat2.setDescription("Descricao alterada");
		// System.out.println(categoryDAO.update(cat2));
		//
		// System.out.println("DELETE");
		// System.out.println(categoryDAO.delete(2));
		//
		// System.out.println("Categoria 2: " + categoryDAO.read(2));
		//
		// // *************** PAGE ***************
		// System.out.println("\n\nPAGE\n\n");
		//
		// Page page1 = new Page();
		// page1.setCategory(cat1);
		// page1.setTitle("PAGINA 1");
		// page1.setContent("Conteudo da pagina 1");
		//
		// Page page2 = new Page();
		// page2.setCategory(cat2);
		// page2.setTitle("PAGINA 2");
		// page2.setContent("Conteudo da pagina 2");
		//
		// PageDAO pageDAO = new PageDAO();
		//
		// // System.out.println("CREATE");
		// pageDAO.create(page1);
		// pageDAO.create(page2);
		//
		// System.out.println("READ ALL");
		// List<Page> pageReadAll = pageDAO.readAll();
		// for (Page page : pageReadAll) {
		// System.out.println(page);
		// }
		//
		// System.out.println("READ");
		// System.out.println(pageDAO.read(2));
		//
		// System.out.println("UPDATE");
		// page2.setContent("Conteudo alterado da pagina 2");
		// System.out.println(pageDAO.update(page2));
		//
		// System.out.println("DELETE");
		// System.out.println(pageDAO.delete(2));
		// System.out.println("Pagina 2: " + pageDAO.read(2));

	}

}